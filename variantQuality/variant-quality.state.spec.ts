import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, TestBed } from '@angular/core/testing';
import { FilterTabDto } from '@api/model/filterTabDto';
import { VariantQualityFilterTabsAction } from '@app/store/variantQuality/variant-quality.actions';
import { VariantQualityState } from '@app/store/variantQuality/variant-quality.state';
import { NgxsModule, Store } from '@ngxs/store';

describe('VariantQuality actions', () => {
  let store: Store;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgxsModule.forRoot([VariantQualityState]),
        HttpClientTestingModule
      ]
    }).compileComponents();
    store = TestBed.get(Store);
  }));

  it('should create an action and add an item', () => {
    store.dispatch(new VariantQualityFilterTabsAction());
    store
      .select((state) => state.variantQuality.defaultFilterTabs)
      .subscribe((defaultFilterTabs: FilterTabDto[]) => {
        expect(defaultFilterTabs).toEqual(
          jasmine.objectContaining([
            { analysisId: '1', sequencingSampleId: '1' }
          ])
        );
      });
  });
});
