import {
  ConfirmUsingPOSTRequestParams,
  GetVariantQualityUsingGETRequestParams
} from '@api';
import { ConfirmationResult } from '@app/store/variantQuality/variant-quality.state';

export class VariantQualityConfirmationAction {
  static readonly type =
    '[VariantQuality] Confirm or reject one or multiple variant qualities';
  constructor(public payload: ConfirmUsingPOSTRequestParams) {}
}

export class VariantQualitySetConfirmationResultAction {
  static readonly type = '[VariantQuality] Set confirmation result';
  constructor(public payload: ConfirmationResult | undefined) {}
}

export class VariantQualityAction {
  static readonly type = '[VariantQuality] Get variant quality list';
  constructor(public payload: GetVariantQualityUsingGETRequestParams) {}
}
