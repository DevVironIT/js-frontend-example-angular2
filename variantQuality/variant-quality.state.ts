import {
  FilterTabDto,
  GetFilterTypesUsingGETRequestParams,
  PageVariantQualityDto,
  VariantQualityControllerService
} from '@api';
import {
  VariantQualityAction,
  VariantQualityConfirmationAction,
  VariantQualitySetConfirmationResultAction
} from '@app/store/variantQuality/variant-quality.actions';
import { Action, Selector, State, StateContext } from '@ngxs/store';

export interface SelectQuickFilter {
  displayName: string;
}

export class VariantQualityStateModel {
  public items: PageVariantQualityDto;
  public confirmationResult: ConfirmationResult | undefined;
}

export interface VariantQualityFilterTypeExpandedPayload {
  queryData: GetFilterTypesUsingGETRequestParams;
  valuesPayload;
}

export enum ConfirmationResult {
  SUCCESS = 'SUCCESS',
  OVERWRITE_WARNING = 'OVERWRITE_WARNING'
}

export const allFilterPreset: FilterTabDto = {
  displayName: 'All',
  filterTypeStates: null,
  selected: true
};

@State<VariantQualityStateModel>({
  name: 'variantQuality',
  defaults: {
    items: undefined,
    confirmationResult: undefined
  }
})
export class VariantQualityState {
  constructor(
    private variantQualityControllerService: VariantQualityControllerService
  ) {}

  @Selector()
  static variantQuality(state: VariantQualityStateModel) {
    return state.items;
  }

  @Selector()
  static confirmationResult(state: VariantQualityStateModel) {
    return state.confirmationResult;
  }

  @Action(VariantQualityConfirmationAction)
  applyOrRejectVariantQuality(
    ctx: StateContext<VariantQualityStateModel>,
    action: VariantQualityConfirmationAction
  ) {
    this.variantQualityControllerService
      .confirmUsingPOST(action.payload)
      .subscribe((confirmationResult) => {
        ctx.patchState({
          confirmationResult: ConfirmationResult[confirmationResult]
        });
      });
  }

  @Action(VariantQualitySetConfirmationResultAction)
  setConfirmationResult(
    ctx: StateContext<VariantQualityStateModel>,
    action: VariantQualitySetConfirmationResultAction
  ) {
    ctx.patchState({
      confirmationResult: action.payload
    });
  }

  @Action(VariantQualityAction)
  getVariantQuality(
    ctx: StateContext<VariantQualityStateModel>,
    action: VariantQualityAction
  ) {
    this.variantQualityControllerService
      .getVariantQualityUsingGET(action.payload)
      .subscribe((variantQualityItems) => {
        ctx.patchState({
          items: variantQualityItems
        });
      });
  }
}
