import { Component, OnInit } from '@angular/core';
import { PageEvent, Sort } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AnalysisDto,
  AnalysisViewDto,
  GetAnalysesUsingGETRequestParams,
  PageAnalysisDto,
  PageAnalysisViewDto
} from '@api';
import { AutocompleteService } from '@app/shared/api/autocomplete.service';
import payloadMapper from '@app/shared/api/payloadMapper';
import {
  FilterValues,
  TableFilterConfig
} from '@app/shared/table-filter/table-filter-utils';
import { AnalysisScreenAction } from '@app/store/analysis-screen/analysis-screen.actions';
import { AnalysisScreenState } from '@app/store/analysis-screen/analysis-screen.state';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

const enum Filters {
  Assays = 'assays',
  AnalysisJob = 'analysis job',
  Tests = 'tests'
}

@Component({
  selector: 'app-interpretations',
  templateUrl: './interpretations.component.html',
  styleUrls: ['./interpretations.component.scss']
})
export class InterpretationsComponent implements OnInit {
  @Select(AnalysisScreenState.analyses) pageAnalysisDefinitionDto$: Observable<
    PageAnalysisViewDto
  >;
  interpretations$: Observable<AnalysisViewDto[]>;

  filterConfig: TableFilterConfig;
  analysesActionPayload: GetAnalysesUsingGETRequestParams = {
    size: 10,
    page: 0,
    analysisBatchDefinitionIds: [],
    assayIds: [],
    sort: [],
    testDefinitionIds: []
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private autocompleteService: AutocompleteService,
    private store: Store
  ) {}

  ngOnInit() {
    this.interpretations$ = this.pageAnalysisDefinitionDto$.pipe(
      filter((s) => !!s),
      map((s) => s.content)
    );

    payloadMapper(this.analysesActionPayload, this.route.snapshot.queryParams);

    this.filterConfig = {
      categories: [
        {
          name: Filters.Assays,
          placeholder: 'Filter by assays...',
          selected$: this.autocompleteService.getAssays$(
            this.analysesActionPayload.assayIds
          ),
          autocomplete: this.autocompleteService.searchAssays$
        },
        {
          name: Filters.AnalysisJob,
          placeholder: 'Filter by batch name...',
          selected$: this.autocompleteService.getBatches$(
            this.analysesActionPayload.analysisBatchDefinitionIds
          ),
          autocomplete: this.autocompleteService.searchBatches$
        },
        {
          name: Filters.Tests,
          placeholder: 'Filter by tests...',
          selected$: this.autocompleteService.getTests$(
            this.analysesActionPayload.testDefinitionIds
          ),
          autocomplete: this.autocompleteService.searchTests$
        }
      ],
      textPlaceholder: '',
      defaultCategory: Filters.AnalysisJob
    };

    this.route.snapshot.queryParamMap.keys.forEach((k) => {
      this.analysesActionPayload[k] = this.route.snapshot.queryParamMap.getAll(
        k
      );
    });

    this.update(true);
  }

  onSort($event: Sort): void {
    this.analysesActionPayload = {
      ...this.analysesActionPayload,
      sort: [`${$event.active},${$event.direction}`]
    };
    this.update();
  }

  onPage($event: PageEvent): void {
    this.analysesActionPayload = {
      ...this.analysesActionPayload,
      page: $event.pageIndex,
      size: $event.pageSize
    };

    this.update();
  }

  onFilter(data: FilterValues): void {
    this.analysesActionPayload = {
      ...this.analysesActionPayload,
      assayIds: data.getValuesByCategory(Filters.Assays) as string[],
      analysisBatchDefinitionIds: data.getValuesByCategory(
        Filters.AnalysisJob
      ) as string[],
      testDefinitionIds: data.getValuesByCategory(Filters.Tests) as string[]
    };

    this.update();
  }

  private update(skipNavigation?: boolean): void {
    this.store.dispatch(new AnalysisScreenAction(this.analysesActionPayload));

    if (!skipNavigation) {
      this.router.navigate([], {
        queryParams: this.analysesActionPayload
      });
    }
  }
}
